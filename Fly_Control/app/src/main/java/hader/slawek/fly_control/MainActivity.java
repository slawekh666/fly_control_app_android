package hader.slawek.fly_control;

import android.support.v4.app.Fragment;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    Button b_button;
    TextView id_textView;

    Animation anim;

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mToggle;
    NavigationView navigationView;
    private Toolbar toolbar;

    FrameLayout frameLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*###############  NAvigation activity button open/close ###############*/

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        ActionBar actionbar = getSupportActionBar();
        ((ActionBar) actionbar).setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //onNavigationItemSelected(0);
        setFragment(0);
        /*toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        frameLayout = (FrameLayout) findViewById(R.id.content_frame);




        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_main);
        //setupDrawerContent(navigationView);
        mToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.open, R.string.close);
        mDrawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //NavigationView navigationView=(NavigationView)findViewById(R.id.navigation_view);
        //navigationView.setNavigationItemSelectedListener(this);

        navigationView = (NavigationView) findViewById(R.id.navigation_view);

        if (navigationView != null) {
            navigationView.setNavigationItemSelectedListener(this);
        }*/


/**##################     Animacja    ############################*/
        //b_button = (Button) findViewById(R.id.b_button);
        //id_textView = (TextView) findViewById(R.id.id_textView);

        /*anim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadein);

        b_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                id_textView.startAnimation(anim);
            }
        });

/** ############################   Painting   #################################*/

        /*Paint paint = new Paint();
        paint.setColor(Color.parseColor("#1C7E17"));
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setStrokeJoin(Paint.Join.BEVEL);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setStrokeWidth(30);
        Display display = getWindowManager().getDefaultDisplay();
        Point point = new Point();
        display.getSize(point);

        Bitmap bitmap = Bitmap.createBitmap(point.x, point.y,
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        canvas.drawRect(10, 200, 200, 400, paint);

        paint.setStrokeWidth(3);
        RectF rectF = new RectF(200, 50, 350, 200);
        canvas.drawArc(rectF, 0, 90, false, paint);
        rectF = new RectF(300, 50, 450, 200);
        canvas.drawArc(rectF, 0, 90, true, paint);
        canvas.drawCircle(550, 125, 75, paint);
        rectF = new RectF(650, 80, 800, 170);
        canvas.drawOval(rectF, paint);
        paint.setStrokeWidth(20);
        canvas.drawLine(850, 200, 1000, 50, paint);

        Rect rect2 = new Rect(0, 0,
                bitmap.getWidth() / 2, bitmap.getHeight() / 2);
        rect2.offset(200, 300);
        paint.setTextSize(100);
        paint.setStrokeWidth(1);
        canvas.drawText("To jest tekst!", 250, 1400, paint);
        /*RelativeLayout relativeLayout =
                (RelativeLayout) findViewById(R.id.relativeLayout);
        if (android.os.Build.VERSION.SDK_INT >=
                Build.VERSION_CODES.JELLY_BEAN)
            relativeLayout.setBackground(
                    new BitmapDrawable(getResources(), bitmap));
        else
            relativeLayout.setBackgroundDrawable(
                    new BitmapDrawable(getResources(), bitmap));*/
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_toolbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }


        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.it_about) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        //int id = item.getItemId();

       // if (id == R.id.it_fly_history) {

            /** otwiera activity we fragmencie*/

           // setFragment(0);

            /** tworzy nowe activity*/
            ///startActivity(new Intent(getApplicationContext(),Setting.class));
            // Handle the camera action
       // }
       // if (id == R.id.it_setting) {
          //  setFragment(1);
            ///startActivity(new Intent(getApplicationContext(),Setting.class));
            // Handle the camera action
       // }

        switch(item.getItemId()){
            case R.id.it_maps:
                setFragment(0);
                break;
            case R.id.it_fly_history:
                setFragment(1);
                break;
            case R.id.it_setting:
                setFragment(2);
                break;
            case R.id.it_about_nav:
                setFragment(3);
                break;
            case R.id.it_help:
                setFragment(4);
                break;
        }
        /*
        FragmentManager fragmentManager;
        FragmentTransaction fragmentTransaction;
        switch (item.getItemId()) {
            case R.id.it_maps:
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                Map mapFragment = new Map();
                fragmentTransaction.replace(R.id.fragment, mapFragment);
                fragmentTransaction.commit();
                break;
            case R.id.it_fly_history:
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                Fly_history flyFragment = new Fly_history();
                fragmentTransaction.replace(R.id.fragment, flyFragment);
                fragmentTransaction.commit();
                break;
            case R.id.it_setting:
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                Setting settingFragment = new Setting();
                fragmentTransaction.replace(R.id.fragment, settingFragment);
                fragmentTransaction.commit();
                break;
            case R.id.it_about_nav:
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                About aboutFragment = new About();
                fragmentTransaction.replace(R.id.fragment, aboutFragment);
                fragmentTransaction.commit();
                break;
            case R.id.it_help:
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                Help helpFragment = new Help();
                fragmentTransaction.replace(R.id.fragment, helpFragment);
                fragmentTransaction.commit();
                break;
        }*/


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



    public void setFragment(int position) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        switch (position) {
            case 0:
                Map mapFragment = new Map();
                fragmentTransaction.replace(R.id.fragment, mapFragment).commit();
                break;
            case 1:
                Fly_history flyFragment = new Fly_history();
                fragmentTransaction.replace(R.id.fragment, flyFragment).commit();
                break;
            case 2:
                Setting settingFragment = new Setting();
                fragmentTransaction.replace(R.id.fragment, settingFragment).commit();
                break;
            case 3:
                About aboutFragment = new About();
                fragmentTransaction.replace(R.id.fragment, aboutFragment).commit();
                break;
            case 4:
                Help helpFragment = new Help();
                fragmentTransaction.replace(R.id.fragment, helpFragment).commit();
                break;

        }
    }


    /*  ########### created and add item clicked option/event  ##############################*/


    /*@Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home: {
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu manu) {
        getMenuInflater().inflate(R.menu.menu_toolbar, manu);
        return true;
    }
/*
    @Override
    public boolean onOptionsItemSelected (MenuItem item){

        if(mToggle.onOptionsItemSelected(item)){


            return true;
        }

        return super.onOptionsItemSelected(item);
    }/


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        /*item.setChecked(true);
        mDrawerLayout.closeDrawers();
        mDrawerLayout.closeDrawers();

        if (item.isChecked()) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
            return false;
        }

        int id = item.getItemId();

        if (id == R.id.it_home) {
            return true;
        }

        if (id == R.id.it_history) {
            return true;
        }

        if (id == R.id.it_settings) {
            startActivity(new Intent(getApplicationContext(),Setting.class));
            return true;
        }

        if (id == R.id.id_about) {
            return true;
        }

        return false;
    }
    
    
    
    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(

                new NavigationView.OnNavigationItemSelectedListener() {

                    @Override

                    public boolean onNavigationItemSelected(MenuItem menuItem) {

                        selectDrawerItem(menuItem);

                        return true;

                    }

                });
    }

    private void selectDrawerItem(MenuItem menuItem) {
        // Create a new fragment and specify the fragment to show based on nav item clicked

        Fragment fragment = null;

        Class fragmentClass;

        switch(menuItem.getItemId()) {

            case R.id.it_settings:

                fragmentClass =Setting.class;

                break;



            default:

                fragmentClass = Setting.class;

        }



        try {

            fragment = (Fragment) fragmentClass.newInstance();

        } catch (Exception e) {

            e.printStackTrace();

        }



        // Insert the fragment by replacing any existing fragment

        FragmentManager fragmentManager = getFragmentManager();

        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();



        // Highlight the selected item has been done by NavigationView

        menuItem.setChecked(true);

        // Set action bar title

        setTitle(menuItem.getTitle());

        // Close the navigation drawer

        mDrawerLayout.closeDrawers();
    }*/
}
