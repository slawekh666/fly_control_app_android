package hader.slawek.fly_control;


import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

public class Setting extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.activity_setting, container, false);

        ((MainActivity) getActivity()).getSupportActionBar().setTitle(R.string.title_activity_setting);



        return view;
    }

    //private DrawerLayout mDrawerLayout;
    //private ActionBarDrawerToggle mToogle;

    /*@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_setting,frameLayout);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //ActionBar actionbar = getSupportActionBar();
        //((ActionBar) actionbar).setDisplayHomeAsUpEnabled(true);

    }

   /* @Override
    protected void onResume() {
        super.onResume();
        // to check current activity in the navigation drawer
        navigationView.getMenu().getItem(0).setChecked(true);
    }
/*
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }*/

}
