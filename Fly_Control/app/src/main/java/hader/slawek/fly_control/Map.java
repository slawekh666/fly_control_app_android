package hader.slawek.fly_control;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class Map extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.activity_map, container, false);

        ((MainActivity) getActivity()).getSupportActionBar().setTitle(R.string.title_activity_map);
        return view;
    }

}
