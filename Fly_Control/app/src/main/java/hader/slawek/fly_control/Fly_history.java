package hader.slawek.fly_control;


import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class Fly_history extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.activity_fly_history, container, false);

        ((MainActivity) getActivity()).getSupportActionBar().setTitle(R.string.title_activity_fly_history);



        return view;
    }
}
